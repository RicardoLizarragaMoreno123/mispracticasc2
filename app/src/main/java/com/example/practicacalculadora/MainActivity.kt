package com.example.practicacalculadora

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AlertDialog
import com.example.practicacalculadora.CalculadoraActivity

class MainActivity : AppCompatActivity() {

    private lateinit var txtUsuario: EditText
    private lateinit var txtContraseña: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)

        btnIngresar.setOnClickListener {
            val intent = Intent(this, CalculadoraActivity::class.java)
            intent.putExtra("usuario", txtUsuario.text.toString())
            startActivity(intent)
        }

        btnSalir.setOnClickListener {
            mostrarDialogoSalir()
        }
    }

    private fun mostrarDialogoSalir() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Desea cerrar la aplicación?")
            .setCancelable(false)
            .setPositiveButton("Cerrar") { dialog, id ->
                finishAffinity() // Cierra la aplicación completamente
            }
            .setNegativeButton("Cancelar") { dialog, id ->
                dialog.dismiss() // Cierra el diálogo y regresa a la actividad
            }
        val alert = builder.create()
        alert.show()
    }
}
