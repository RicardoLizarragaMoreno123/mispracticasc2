package com.example.practicacalculadora

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class CalculadoraActivity : AppCompatActivity() {

    private lateinit var lblResultado: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var btnSuma: Button
    private lateinit var btnResta: Button
    private lateinit var btnMult: Button
    private lateinit var btnDiv: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        lblResultado = findViewById(R.id.lblResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)
        btnMult = findViewById(R.id.btnMult)
        btnDiv = findViewById(R.id.btnDiv)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        btnSuma.setOnClickListener { verificarYCalcular('+') }
        btnResta.setOnClickListener { verificarYCalcular('-') }
        btnMult.setOnClickListener { verificarYCalcular('*') }
        btnDiv.setOnClickListener { verificarYCalcular('/') }
        btnLimpiar.setOnClickListener { limpiarCampos() }
        btnRegresar.setOnClickListener { finish() }
    }

    private fun verificarYCalcular(operador: Char) {
        val num1Text = txtNum1.text.toString()
        val num2Text = txtNum2.text.toString()

        if (num1Text.isEmpty() || num2Text.isEmpty()) {
            Toast.makeText(this, "Falta capturar datos", Toast.LENGTH_SHORT).show()
        } else {
            calcular(operador)
        }
    }

    private fun calcular(operador: Char) {
        val num1 = txtNum1.text.toString().toFloatOrNull()
        val num2 = txtNum2.text.toString().toFloatOrNull()

        if (num1 == null || num2 == null) {
            lblResultado.text = "Error: Entrada no válida"
            return
        }

        var resultado = 0f

        when (operador) {
            '+' -> resultado = num1 + num2
            '-' -> resultado = num1 - num2
            '*' -> resultado = num1 * num2
            '/' -> {
                if (num2 != 0f) {
                    resultado = num1 / num2
                } else {
                    lblResultado.text = "Error: División por cero"
                    return
                }
            }
        }

        lblResultado.text = resultado.toInt().toString()
    }

    private fun limpiarCampos() {
        txtNum1.text.clear()
        txtNum2.text.clear()
        lblResultado.text = ""
    }
}
